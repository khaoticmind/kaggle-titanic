# -*- coding: utf-8 -*-
"""
Created on Mon May 18 13:25:28 2015

@author: Mkhaos7
"""

import sys
import pandas as pd
import numpy as np
from scipy.sparse import hstack
from sklearn import svm
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import normalize
from sklearn.feature_extraction import FeatureHasher
from sklearn.ensemble import RandomForestClassifier, BaggingClassifier, ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA

from sklearn.base import BaseEstimator, ClassifierMixin

class EnsembleClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, classifiers=None):
        self.classifiers = classifiers

    def fit(self, X, y):
        for classifier in self.classifiers:
            classifier.fit(X, y)

    def predict_proba(self, X):
        self.predictions_ = list()
        for classifier in self.classifiers:
            self.predictions_.append(classifier.predict_proba(X))
        return np.mean(self.predictions_, axis=0)

    def predict(self, X):
        self.predictions_ = list()
        for classifier in self.classifiers:
            self.predictions_.append(classifier.predict_proba(X))
        mean =  np.mean(self.predictions_, axis=0)
        return np.argmax(mean, axis=1)

def readFullSet():
    test = pd.read_csv('titanic_full.csv')

    test['Survived'] = test.survived
    test['Pclass'] = test.pclass
    test['Name'] = test.name
    test['Sex'] = test.sex
    test['Age'] = test.age
    test['SibSp'] = test.sibsp
    test['Parch'] = test.parch
    test['Ticket'] = test.ticket
    test['Fare'] = test.fare
    test['Cabin'] = test.cabin
    test['Embarked'] = test.embarked

    test = test.drop(['pclass', 'survived', 'name', 'sex', 'age', 'sibsp', 'parch',
                  'ticket','fare', 'cabin', 'embarked', 'boat', 'body', 'home.dest'], axis=1)
    return test

def readTest():
    test = pd.read_csv('titanic_test.csv')
    test = test.set_index('PassengerId')

    return test

def readTrain():
    train = pd.read_csv('titanic_train.csv')
    train = train.set_index('PassengerId')

    return train

def doTest():
    train = readTrain()
    test = readFullSet()

    train_data, train_target  = processDataSet(train)
    test_data, test_target = processDataSet(test)

    classifiers = {}
    classifiers['rf'] = RandomForestClassifier()

    classifiers['bagg-tree'] = BaggingClassifier(max_features= 0.5, max_samples=0.7, n_estimators=500)

    classifiers['adaboost'] = AdaBoostClassifier(n_estimators=1000, algorithm="SAMME")

    classifiers['svm-poly'] = svm.SVC(kernel='poly', C=0.1, coef0=3, degree=6, gamma=2.0)

    classifiers['knn'] = KNeighborsClassifier()
    classifiers['bagg-knn'] = BaggingClassifier(base_estimator=KNeighborsClassifier(),
                                            max_features= 0.8, bootstrap_features= True, n_estimators=20)
    #classifiers['svm-svc-rbf'] = svm.SVC(kernel='rbf')
    #classifiers['svm-svc-sigmoid'] = svm.SVC(kernel='sigmoid')

    classifiers['ensemble'] = EnsembleClassifier(
                                [svm.SVC(kernel='poly', C=0.1, coef0=3, degree=4, gamma=2.0, probability=True),
                                 svm.SVC(kernel='poly', C=0.1, coef0=3, degree=6, gamma=2.0, probability=True),
                                svm.SVC(kernel='poly', C=0.1, coef0=3, degree=5, gamma=2.0, probability=True),])

    do_grid_search = False

    if do_grid_search:
        rf_parameters = { 'criterion': ('gini', 'entropy'),
                         'n_estimators' :[20, 30, 40],
                         'max_features': [0.3, 0.4, 0.5],
                        'min_samples_split' : [1]}

        svm_svc_parameters = { 'C':     [0.1, 0.5, 1.0, 1.5, 2.0, 2.5 , 3.0],
                               'gamma': [0.0, 0.1, 0.5, 1.0, 1.5, 2.0, 2.5 , 3.0],
                              'coef0' : [0.0, 0.1, 0.5, 1.0, 1.5, 2.0, 2.5 , 3.0],
                               'degree': [4, 5, 6, 7]}

        knn_parameters = {'n_neighbors': [3, 5, 10, 15, 20],
                          #'algorithm' : [ 'ball_tree', 'kd_tree', 'brute'],
                            'p':[1,2,3,4] }


        grid = GridSearchCV(svm.SVC() , svm_svc_parameters, n_jobs=7, verbose=1, cv=10)
        grid.fit(train_data.toarray(), train_target)
        print(grid.best_params_)
        print(grid.best_score_)

        #classifiers['grid-rf'] = GridSearchCV(RandomForestClassifier(), rf_parameters)
        #grid_rf.fit(train_data, train_target)

    else:
        for name in classifiers.keys():
            clf = classifiers[name]

            #print("Result for {}".format(name))
            #clf.fit(train_data, train_target)
            #test_predict = clf.predict(test_data)
            #print(classification_report(test_target, test_predict))

            res = cross_val_score(clf, train_data, train_target, n_jobs=7, cv=10)
            print("{} CrossValidation = {:.3f} +/- {:.3f}".format(name, np.mean(res), np.std(res)))
            print("--------------")


def doProcess():
    train = readTrain()
    test = readTest()

    train_data, train_target = processDataSet(train)
    test_data, test_target = processDataSet(test)

    clf = svm.SVC(kernel='poly', C=1, coef0=0.5, degree=5, gamma=3.0, probability=True)

    clf.fit(train_data, train_target)
    pred = clf.predict(test_data)

    pred = np.array(pred)

    res = pd.DataFrame(data=pred, index=test.index, columns=['Survived'])

    res.to_csv('test_result.csv')

def processDataSet(dataSet_):
    dataSet = dataSet_.copy()
    #dataSet = dataSet[dataSet['Fare'] < (dataSet.Fare.mean() + 2 * dataSet.Fare.std())]
    #dataSet = dataSet[dataSet['Age'] < (dataSet.Age.mean() + 2 * dataSet.Age.std())]

    #Select the columns we group by
    if 'Survived' in dataSet.columns:
        group_set = ['Survived', 'Sex', 'Pclass']
    else:
        group_set = ['Sex', 'Pclass']

    # he code bellow is used to replace the missing entries
    dataSet['Age'] = dataSet.groupby(group_set)['Age'].\
                                apply(lambda x: x.fillna(x.mean()))

    dataSet['Fare'] = dataSet.groupby(group_set)['Fare'].\
                                apply(lambda x: x.fillna(x.mean()))

    dataSet['Embarked'] = dataSet['Embarked'].fillna(dataSet['Embarked'].mode()[0])

    #Extract the "name" from the tickets
    dataSet['ticket_name'] = dataSet['Ticket'].apply(lambda x:
                                        x.split()[0].replace(".", "").replace("/", "")
                                        if len(x.split()) > 1
                                        else "-")

    dataSet['age-pclass'] = dataSet['Age'] * dataSet['Pclass']
    dataSet['sex-weights'] = dataSet['Sex'].map({'male': 0.5, 'female': 4})
    dataSet['sex-pclass'] = dataSet['sex-weights'] * (4-dataSet['Pclass'])
    dataSet['sex-age-class'] = dataSet['sex-weights'] * (1/dataSet['Age']) * (4-dataSet['Pclass'])
    dataSet['last_name'] = dataSet['Name'].str.extract('(^[A-Za-z]+),\ +([a-zA-Z]+)\.')[0].astype('str')
    dataSet['title'] = dataSet['Name'].str.extract('(^[A-Za-z]+),\ +([a-zA-Z]+)\.')[1].astype('str')

    dataSet['Fare'] = dataSet['Fare'] / (dataSet['SibSp'] + dataSet['Parch'] + 1)
    dataSet['FamilySize'] = dataSet['SibSp'] + dataSet['Parch']

    hasher = FeatureHasher(n_features= 1024, input_type='string')
    disc_feat = None
    for col in ['Sex', 'ticket_name', 'title', 'last_name']: #ticket, embarked 'Sex', 'ticket_name', 'title', 'last_name'
        h = hasher.transform(dataSet[col])
        if disc_feat is None:
            disc_feat = h
        else:
            disc_feat = disc_feat + h

    #sex_dict = (dataSet.groupby("Sex")['Survived'].sum() / dataSet.groupby("Sex")['Survived'].count()).to_dict()
    dataSet['Sex_S'] = dataSet['Sex'].map({'female': 0.7420382165605095, 'male': 0.18890814558058924})
    dataSet['Sex_S'] = dataSet['Sex_S'].astype('float32')

    #embarked_dict = (dataSet.groupby("Embarked")['Survived'].sum() / dataSet.groupby("Embarked")['Survived'].count()).to_dict()
    dataSet['Embarked_S'] = dataSet['Embarked'].map({'C': 0.5535714285714286, 'Q': 0.38961038961038963, 'S': 0.33695652173913043})
    dataSet['Embarked_S'] = dataSet['Embarked_S'].astype('float32')

    #title_dict = (dataSet.groupby("title")['Survived'].sum() / dataSet.groupby("title")['Survived'].count()).to_dict()
    #dataSet['title_S'] = dataSet['title'].map(title_dict)
    #dataSet['title_S'] = dataSet['title_S'].astype('float32')

    if 'Survived' in dataSet.columns:
        target = dataSet['Survived'].values
        data = dataSet.drop(['Name', 'Embarked', 'Ticket', 'Survived','Cabin', 'Sex',
                             'sex-weights', 'ticket_name', 'last_name', 'title'], axis=1).values

    else:
        target= None
        data = dataSet.drop(['Name', 'Embarked', 'Ticket','Cabin', 'Sex',
                             'sex-weights', 'ticket_name', 'last_name', 'title'], axis=1).values

    data = hstack([disc_feat, data])


    data = normalize(data, axis=1, norm='l2')

    return data, target

def doKeras():
    from keras.models import Sequential
    from keras.utils import np_utils
    from keras.layers.core import Dense, Activation, Dropout
    from keras.optimizers import SGD

    train = readTrain()
    test = readTest()

    train_data, train_target = processDataSet(train)
    test_data, test_target = processDataSet(test)

    train_target = np_utils.to_categorical(train_target)

    input_dim = train_data.shape[1]
    nb_classes = train_target.shape[1]

    # Here's a Deep Dumb MLP (DDMLP)
    model = Sequential()
    model.add(Dense(input_dim, 128, init='lecun_uniform', activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(128, 128, init='lecun_uniform', activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(128, 128, init='lecun_uniform', activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(128, 128, init='lecun_uniform', activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(128, 128, init='lecun_uniform', activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(128, 128, init='lecun_uniform', activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(128, nb_classes, init='lecun_uniform', activation='relu'))

    model.compile(loss='mse', optimizer='adam')

    print("Training...")
    model.fit(train_data.toarray(), train_target, nb_epoch=500, batch_size=8, verbose=2, show_accuracy=True)


    pred = model.predict_classes(test_data.toarray(), verbose=1)
    pred = np.array(pred)
    res = pd.DataFrame(data=pred, index=test.index, columns=['Survived'])
    res.to_csv('test_result.csv')


#doTest()
#doProcess()

doKeras()
